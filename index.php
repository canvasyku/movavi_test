<?php
// require_once 'autoload.php';
require __DIR__ . '/vendor/autoload.php';

use GetAvgExchangeRate\Builder;

// Это тестовый пример использования библиотеки
$avgExchange = new Builder(new DateTime('2019-02-20'));
$result = $avgExchange->run();

var_dump($result);