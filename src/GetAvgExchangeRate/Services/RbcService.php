<?php
namespace GetAvgExchangeRate\Services;

use GetAvgExchangeRate\Exceptions\{
    ServiceUnavailableException,
    WrongResponseException,
    ExtensionNotFoundException
};

/**
 * @package RbcService - сервис получения курсов валют с rbc.ru
 */
class RbcService implements ServiceInterface {

    /**
     * базовый урл для запросов
     */
    const URL = 'https://cash.rbc.ru/cash/json/converter_currency_rate/';

    /**
     * Формат даты используемый в запросах
     */
    const DATE_FORMAT = 'Y-m-d';

    /**
     * Проверяет необходимые библиотеки для работы сервиса
     *
     * @throws \GetAvgExchangeRate\Exceptions\ExtensionNotFoundException
     */
    private static function checkLibs (): void {
        if (!function_exists('json_decode')) {
            throw new ExtensionNotFoundException('json');
        }
    }

    /**
     * Отправляет запрос на сервер и возвращает ответ
     *
     * @param String $currency
     * @param \DateTime $date
     *
     * @return String
     *
     * @throws \GetAvgExchangeRate\Exceptions\ServiceUnavailableException
     */
    private static function sendHttpRequest (String $currency, \DateTime $date): String {
        $queryArray = array(
            'currency_from' => $currency,
            'currency_to' => 'RUR',
            'source' => 'cbrf',
            'sum' => 1,
            'date' => $date->format(self::DATE_FORMAT)
        );
        $query = http_build_query($queryArray);

        $requestResult = file_get_contents(self::URL . "?" . $query);

        if ($requestResult === false) {
            throw new ServiceUnavailableException();
        }

        return $requestResult;
    }

    /**
     * Достаёт из ответа от сервера значение курса
     *
     * @param String $response
     *
     * @return Float
     *
     * @throws \GetAvgExchangeRate\Exceptions\WrongResponseException
     */
    private static function parseResponse(String $response): Float {
        $json = json_decode($response);

        if (!isset($json->data->sum_result)) {
            throw new WrongResponseException('->data->sum_result', $response);
        }

        return (float)$json->data->sum_result;
    }

    /**
     * @param Array $currencies - валюты, курсы которых необходимо получить
     * @param \DateTime $date - дата, на которую надо получить курс
     *
     * @return Array - ['USD' => 66.2022, ...]
     *
     * @throws \GetAvgExchangeRate\Exceptions\ServiceUnavailableException
     * @throws \GetAvgExchangeRate\Exceptions\WrongResponseException
     */
    public static function run (Array $currencies, \DateTime $date): Array {
        self::checkLibs();
        $result = array();

        foreach ($currencies as $currency) {
            $requestResult = self::sendHttpRequest($currency, $date);

            $result[$currency] = self::parseResponse($requestResult);
        }

        return $result;
    }
}
?>