<?php
namespace GetAvgExchangeRate\Services;

/**
 * @param Array $currencies - валюты, курсы которых необходимо получить
 * @param \DateTime $date - дата, на которую надо получить курс
 * 
 * @return Array - ['USD' => 66.2022, ...]
 */
interface ServiceInterface {
    public static function run(Array $currency, \DateTime $date): Array;
}