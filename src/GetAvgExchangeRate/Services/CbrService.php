<?php
namespace GetAvgExchangeRate\Services;

use GetAvgExchangeRate\Exceptions\{
    ExtensionNotFoundException,
    ServiceUnavailableException,
    WrongResponseException,
    EmptyResponseException,
    UnsupportedCurrencyException
};

/**
 * @package CbrService - сервис получения курсов валют с cbr.ru
 */
class CbrService implements ServiceInterface {

    /**
     * Базовый url для запросов
     */
    const URL = 'http://www.cbr.ru/scripts/XML_dynamic.asp';

    /**
     * Формат даты для сервиса
     */
    const DATE_FORMAT = 'd/m/Y';

    /**
     * Массив соответствия кодов валют к кодам сервиса
     */
    const CURRENCY_CODES = [
        'USD' => 'R01235',
        'EUR' => 'R01239'
    ];

    /**
     * Проверяет необходимые библиотеки для работы сервиса
     *
     * @throws \GetAvgExchangeRate\Exceptions\ExtensionNotFoundException
     */
    private static function checkLibs (): void {
        if (!function_exists('simplexml_load_string')) {
            throw new ExtensionNotFoundException('SimpleXml');
        }
    }

    /**
     * Отправляет запрос на сервер и возвращает ответ
     *
     * @param String $currency
     * @param \DateTime $date
     *
     * @return String
     *
     * @throws \GetAvgExchangeRate\Exceptions\ServiceUnavailableException
     */
    private static function sendHttpRequest (String $currency, \DateTime $date): String {
        $queryArray = array(
            'date_req1' => $date->format(self::DATE_FORMAT),
            'date_req2' => $date->format(self::DATE_FORMAT),
            'VAL_NM_RQ' => self::CURRENCY_CODES[$currency]
        );
        $query = http_build_query($queryArray);

        $requestResult = file_get_contents(self::URL . "?" . $query);

        if ($requestResult === false) {
            throw new ServiceUnavailableException();
        }

        return $requestResult;
    }

    /**
     * Достаёт из ответа от сервера значение курса
     *
     * @param String $response
     *
     * @return Float
     *
     * @throws \GetAvgExchangeRate\Exceptions\EmptyResponseException
     * @throws \GetAvgExchangeRate\Exceptions\WrongResponseException
     */
    private static function parseResponse(String $response): Float {
        if (!$response) {
            throw new EmptyResponseException();
        }

        $xml = simplexml_load_string($response);

        if (!isset($xml->Record->Value[0])) {
            throw new WrongResponseException('->Record->Value[0]', json_encode($xml));
        }

        $value = (string)$xml->Record->Value[0];

        return (float)str_replace(',', '.', $value);
    }

    /**
     * @param Array $currencies - валюты, курсы которых необходимо получить
     * @param \DateTime $date - дата, на которую надо получить курс
     *
     * @return Array - ['USD' => 66.2022, ...]
     *
     * @throws \GetAvgExchangeRate\Exceptions\ExtensionNotFoundException
     * @throws \GetAvgExchangeRate\Exceptions\ServiceUnavailableException
     * @throws \GetAvgExchangeRate\Exceptions\WrongResponseException
     * @throws \GetAvgExchangeRate\Exceptions\EmptyResponseException
     * @throws \GetAvgExchangeRate\Exceptions\UnsupportedCurrencyException
     */
    public static function run (Array $currencies, \DateTime $date): Array {
        self::checkLibs();

        $result = array();

        foreach ($currencies as $currency) {
            if (!array_key_exists($currency, self::CURRENCY_CODES)) {
                throw new UnsupportedCurrencyException();
            }

            $requestResult = self::sendHttpRequest($currency, $date);

            $result[$currency] = self::parseResponse($requestResult);
        }

        return $result;
    }

}