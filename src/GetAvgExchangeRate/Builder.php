<?php
namespace GetAvgExchangeRate;

use GetAvgExchangeRate\Exceptions\DateInFutureException;

class Builder {

    const AVAILABLE_CORRENCIES = [
        "EUR",
        "USD"
    ];

    const SERVICES_LIST = [
        'RbcService',
        'CbrService'
    ];

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var Array
     */
    private $currencies;

    /**
     * @param DateTime $date
     * @param Array $currencies = []
     *
     * @throws \GetAvgExchangeRate\Exceptions\DateInFutureException
     */
    function __construct(\DateTime $date ) {
        self::verifyParams($date);

        $this->date = $date;
        $this->currencies = self::AVAILABLE_CORRENCIES;
    }

    /**
     * Проверяет входные параметры
     *
     * @throws \GetAvgExchangeRate\Exceptions\DateInFutureException
     */
    private static function verifyParams(\DateTime $date ) {
        $now = new \DateTime();
        if ($date > $now) {
            throw new DateInFutureException;
        }
    }

    /**
     * Выполняет запросы к сервисам валют и возвращает массив результатом
     *
     * @return Array ['USD' => 66.2022, ...]
     *
     * @throws \GetAvgExchangeRate\Exceptions\ExtensionNotFoundException
     * @throws \GetAvgExchangeRate\Exceptions\EmptyResponseException
     * @throws \GetAvgExchangeRate\Exceptions\ServiceUnavailableException
     * @throws \GetAvgExchangeRate\Exceptions\WrongResponseException
     * @throws \GetAvgExchangeRate\Exceptions\UnsupportedCurrencyException
     */
    public function run(): Array {
        $result = [];
        $servicesResult = [];

        // делаем запросы и суммируем сразу результаты для производительности
        foreach (self::SERVICES_LIST as $service) {
            $resultService = call_user_func("GetAvgExchangeRate\\Services\\${service}::run", $this->currencies, $this->date);
            $servicesResult[$service] = $resultService;

            foreach ($this->currencies as $currency) {
                if (!array_key_exists($currency, $result)) {
                    $result[$currency] = 0;
                }

                $result[$currency] += $resultService[$currency];
            }
        }

        // меняем результат на среднее арифметическое
        foreach ($result as $key => $currency) {
            $result[$key] = $currency / count(self::SERVICES_LIST);
        }

        return $result;
    }

}
?>