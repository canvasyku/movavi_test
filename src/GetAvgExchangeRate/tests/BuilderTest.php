<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use GetAvgExchangeRate\Builder;

final class BuilderTest extends TestCase {
    /**
     * @expectedException GetAvgExchangeRate\Exceptions\DateInFutureException
     */
    public function testDateInFutureException(): void
    {
        $test = new Builder(new \DateTime('2020-10-10'));
    }

    public function testResponseKeysAndType(): void {
        $builder = new Builder(new \DateTime('2018-10-10'));
        $result = $builder->run();
        $this->assertArrayHasKey('EUR', $result);
        $this->assertArrayHasKey('USD', $result);
        $this->assertInternalType('float', $result['EUR']);
        $this->assertInternalType('float', $result['USD']);
    }

    // public function testVerifyParamsCalls(): void {
    //     // $mock = $this->getMockBuilder(Builder::class)
    //     //     ->setMethods(['verifyParams'])
    //     //     ->disableOriginalConstructor()
    //     //     ->getMock();

    //     // $mock->expects($this->once())->method('verifyParams');
    //     // // $mock->expects($this->any())
    //     // //     ->method('verifyParams')
    //     // //     ->will($this->returnValue(''));

    //     // $mock->__construct(new \DateTime('2018-10-10'));


    //     $mock = $this->getMockBuilder(Builder::class)
    //         ->disableOriginalConstructor()
    //         ->setConstructorArgs([new \DateTime('2018-10-10')])
    //         ->setMethods(['verifyParams'])
    //         ->getMock();

    //     $mock->expects($this->once())
    //         ->method('verifyParams')
    //         ->with($this->equalTo(new \DateTime('2018-10-10')));

    //     $reflectedClass = new ReflectionClass(Builder::class);
    //     $constructor = $reflectedClass->getConstructor();
    //     $constructor->invoke($mock, new \DateTime('2018-10-10'));
    // }
}