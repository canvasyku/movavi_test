<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use GetAvgExchangeRate\Services\CbrService;


final class CbrServiceTest extends TestCase {

    /*
    public function testRun() {
        override_function('file_get_contents', '', 'return override_file_get_contents();');
        function override_file_get_contents(){
            return '<?xml version="1.0" encoding="windows-1251"?><ValCurs ID="R01235" DateRange1="10.10.2018" DateRange2="10.10.2018" name="Foreign Currency Market Dynamic"><Record Date="10.10.2018" Id="R01235"><Nominal>1</Nominal><Value>66,4032</Value></Record></ValCurs>'; 
        }

        $result = CbrService::run(['USD'], new \DateTime('2018-10-10'));
        $this->assertEquals(['USD' => 66.4032], $result);
    }*/

    /**
     * @expectedException GetAvgExchangeRate\Exceptions\UnsupportedCurrencyException
     */
    public function testRunWrongParameters(): void {
        CbrService::run([''], new \DateTime('2018-10-10'));
    }

}