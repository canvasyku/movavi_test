<?php
namespace GetAvgExchangeRate\Exceptions;

class UnsupportedCurrencyException extends \Exception {
    public $message = 'Currency Unsupported';
}