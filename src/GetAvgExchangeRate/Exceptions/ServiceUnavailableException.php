<?php
namespace GetAvgExchangeRate\Exceptions;

class ServiceUnavailableException extends \Exception {
    public $message = 'Service Unavailable';
}