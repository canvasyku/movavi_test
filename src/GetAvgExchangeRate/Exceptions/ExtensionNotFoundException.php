<?php
namespace GetAvgExchangeRate\Exceptions;

class ExtensionNotFoundException extends \Exception {
    function __construct ($extensionName) {
        parent::__construct();
        $this->message = "$extensionName not found";
    }
}