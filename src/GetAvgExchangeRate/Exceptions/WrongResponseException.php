<?php
namespace GetAvgExchangeRate\Exceptions;

class WrongResponseException extends \Exception {
    function __construct (String $expected, String $recived) {
        parent::__construct();
        $this->message = "Expected: $expected,". PHP_EOL ." Recived: $recived";
    }
}