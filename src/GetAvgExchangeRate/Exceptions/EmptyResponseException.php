<?php
namespace GetAvgExchangeRate\Exceptions;

class EmptyResponseException extends \Exception {
    public $message = 'Empty response from service';
}