<?php
namespace GetAvgExchangeRate\Exceptions;

class DateInFutureException extends \Exception {
    public $message = 'Date in Future';
}