# movavi_test

## Install
Install composer: https://getcomposer.org/download/

Install dependencies:
```bash
php composer.phar install
```

## RUN example

```bash
php index.php
```

## Tests

### Install

```bash
php composer.phar install --dev
```

### Run

```bash
./vendor/bin/phpunit --bootstrap vendor/autoload.php src/GetAvgExchangeRate/tests
```